import 'package:rly/api/app.api.dart';
import 'package:rly/datasource/request_param/create_photo_chat.request_param.dart';
import 'package:rly/datasource/response/photo_post.response.dart';

class PhotoPostDataSource {
  static PhotoPostDataSource shared = PhotoPostDataSource._();
  PhotoPostDataSource._();

  Future<List<PhotoPostResponse>> getPhotoPosts(
          {required int page, required String? token}) =>
      AppApi.shared.getPhotoPosts(page: page, token: token);

  Future<PhotoPostResponse> createPhotoPost(
          {required CreatePhotoChatRequestParam param,
          required String? token}) =>
      AppApi.shared.createPhotoPost(param: param, token: token);
}
