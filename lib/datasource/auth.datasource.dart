import 'package:rly/api/auth.api.dart';
import 'package:rly/datasource/response/user_auth.response.dart';

class AuthDataSource {
  static AuthDataSource shared = AuthDataSource._();
  AuthDataSource._();

  UserAuth? get signedUser => AuthApi.shared.signedUser;

  Future<void> signOutUser() async => AuthApi.shared.signOutUser();

  Future<UserAuth?> signInUser() async => AuthApi.shared.signInUser();
}
