import 'package:rly/datasource/response/photo.response.dart';
import 'package:rly/datasource/response/photo_chat.response.dart';
import 'package:rly/datasource/response/response.dart';
import 'package:rly/repository/entities/photo_post.entity.dart';

class PhotoPostResponse implements ApiResponse<PhotoPost> {
  final String id;
  final List<PhotoChatResponse> photoChats;
  final PhotoResponse photo;
  final DateTime updatedAt;
  final int unreadConversationsCount;

  const PhotoPostResponse({
    required this.id,
    required this.photoChats,
    required this.updatedAt,
    required this.photo,
    required this.unreadConversationsCount,
  });

  @override
  PhotoPost get toEntity => PhotoPost(
        id: id,
        photoChats: photoChats.map((chat) => chat.toEntity).toList(),
        updatedAt: updatedAt,
        photo: photo.toEntity,
        unreadConversationsCount: unreadConversationsCount,
      );
}
