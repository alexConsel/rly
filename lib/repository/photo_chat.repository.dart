import 'package:rly/datasource/auth.datasource.dart';
import 'package:rly/datasource/photo_chat.datasource.dart';
import 'package:rly/repository/entities/photo_chat.entity.dart';

class PhotoChatRepository {
  static PhotoChatRepository shared = PhotoChatRepository._();
  PhotoChatRepository._();

  Future<List<PhotoChat>> getPhotoChats({required int page}) =>
      PhotoChatDataSource.shared
          .getPhotoChats(
              page: page, token: AuthDataSource.shared.signedUser?.token)
          .then((photoChats) =>
              photoChats.map((photoChat) => photoChat.toEntity).toList());

  Future<void> addTextMessage({required String message}) =>
      PhotoChatDataSource.shared.addTextMessage(
          message: message, token: AuthDataSource.shared.signedUser?.token);

  Future<void> readPhotoChat({required String photoChatId}) =>
      PhotoChatDataSource.shared.readPhotoChat(
          photoChatId: photoChatId,
          token: AuthDataSource.shared.signedUser?.token);
}
