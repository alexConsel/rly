import 'package:go_router/go_router.dart';

abstract class AppRoute {
  String get path;
  RouteBase get route;
}
