part of '../dashboard.page.dart';

//TODO: this could be factorized with FriendsRLYPage
class MyRLYPage extends ConsumerStatefulWidget {
  final double modalHeight;
  final User authUser;

  const MyRLYPage({
    required this.authUser,
    required this.modalHeight,
    super.key,
  });

  @override
  ConsumerState<MyRLYPage> createState() => _MyRLYPageState();
}

class _MyRLYPageState extends ConsumerState<MyRLYPage>
    with AutomaticKeepAliveClientMixin<MyRLYPage> {
  late final _notifier = ref.read(asyncPhotoPostProvider.notifier);

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_) {
      _fetchMorePosts();
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    final asyncPhotoPost = ref.watch(asyncPhotoPostProvider);

    final itemCount = (asyncPhotoPost.value?.length ?? 0) + 1;
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(AppSize.l.value),
        child: switch (asyncPhotoPost) {
          AsyncData<List<PhotoPost>>() || AsyncLoading() => GridView.builder(
              clipBehavior: Clip.none,
              itemCount: (asyncPhotoPost.value?.length ?? 0) + 1,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                mainAxisSpacing: AppSize.l.value,
                crossAxisSpacing: AppSize.l.value,
                childAspectRatio: 1,
              ),
              itemBuilder: (BuildContext context, int index) {
                if (index == itemCount - 1) {
                  return _notifier.hasMoreContent
                      ? VisibilityDetector(
                          key: const Key('rly_loader'),
                          child: const LoaderWidget(),
                          onVisibilityChanged: (_) => _fetchMorePosts(),
                        )
                      : const SizedBox.shrink();
                }

                final photoPost = asyncPhotoPost.value![index];
                return RLYPreviewWidget(
                  notificationCount: photoPost.unreadConversationsCount,
                  onTapUp: () => _openPhotoConversationList(photoPost),
                  url: photoPost.photo.url.toString(),
                );
              },
            ),
          _ => ErrorPage(
              errorMessage: 'error...',
              onReload: () => ref.invalidate(asyncPhotoPostProvider),
            )
        },
      ),
      floatingActionButton: AppButton.builder(
        isLoading: false,
        onTapUp: () => showModalBottomSheet<Photo?>(
          context: context,
          builder: (BuildContext context) {
            return ImageSelectDropDownWidget(
              height: MediaQuery.of(context).size.height * 0.8,
            );
          },
        ).then((photo) => photo == null ? null : _openPublishPhoto(photo)),
        child: const _AddButtonWidget(),
      ),
    );
  }

  void _fetchMorePosts() => _notifier.hasMoreContent
      ? _notifier
          .fetchMorePhotoPosts()
          .onError((error, stackTrace) => context.showMessage(error.toString()))
      : null;

  void _openPhotoConversationList(PhotoPost photoPost) =>
      showModalBottomSheet<PhotoChat>(
        context: context,
        builder: (BuildContext context) {
          return ConversationDropDown(
            authUser: widget.authUser,
            photoPost: photoPost,
            height: widget.modalHeight,
          );
        },
      ).then(
        (photoChat) => photoChat == null
            ? null
            : _openPhotoChat(photoChat: photoChat, photoPost: photoPost),
      );

  void _openPhotoChat(
      {required PhotoChat photoChat, required PhotoPost photoPost}) {
    _notifier
        .markPhotoChatAsRead(
      photoChat: photoChat,
      photoPost: photoPost,
    )
        .onError((error, stackTrace) {
      //do nothing
    });

    context.push<PhotoChat>(
      ChatRoute().path,
      extra: PhotoPostParam(
        photo: photoChat.photo,
        chatId: photoChat.id,
        postId: photoPost.id,
        authUser: widget.authUser,
      ),
    );
  }

  void _openPublishPhoto(Photo photo) {
    showDialog<Photo>(
      context: context,
      builder: (context) => Dialog(
        child: PublishPage(
          photo: photo,
        ),
      ),
    ).then(_publishPhoto);
  }

  void _publishPhoto(photo) => photo == null
      ? null
      : _notifier
          .createPhotoPost(photo: photo)
          .then((_) => context.showMessage('your RLY got shipped 🎉'))
          .onError(
              (error, stackTrace) => context.showMessage(error.toString()));

  @override
  bool get wantKeepAlive => true;
}
