import 'package:rly/datasource/response/user_auth.response.dart';

const _currentUser = UserAuth(userId: '0', token: 'fas8hee');

class AuthApi {
  static AuthApi shared = AuthApi._();

  AuthApi._();

  UserAuth? get signedUser => _currentUser;

  Future<void> signOutUser() async => Future.value();

  Future<UserAuth?> signInUser() async => _currentUser;
}
