enum AppSizing {
  xxs,
  xs,
  s,
  m,
  l,
  xl,
}
