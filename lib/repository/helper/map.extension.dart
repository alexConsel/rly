extension MapIndexed<T, E> on Map<T, E> {
  Iterable<R> mapIndexed<R>(R Function(int index, T key, E value) f) sync* {
    var index = 0;
    for (var entry in entries) {
      yield f(index++, entry.key, entry.value);
    }
  }
}
