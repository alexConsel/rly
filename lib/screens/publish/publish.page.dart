import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:rly/design_system/app_button/app_button.dart';
import 'package:rly/design_system/app_separators.dart';
import 'package:rly/design_system/app_size.dart';
import 'package:rly/repository/entities/photo.entity.dart';
import 'package:rly/screens/common/widgets/photo.widget.dart';
import 'package:twemoji/twemoji.dart';

class PublishPage extends StatelessWidget {
  final Photo photo;

  const PublishPage({required this.photo, super.key});

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(AppSize.m.value),
      child: Scaffold(
        appBar: AppBar(
          title: TwemojiText(
            text: 'looking good ✨',
            style: Theme.of(context).appBarTheme.titleTextStyle,
          ),
          backgroundColor: Colors.transparent,
          automaticallyImplyLeading: false,
          centerTitle: true,
          actions: [
            AppButton.builder(
              isLoading: false,
              onTapUp: () => context.pop(),
              child: const Icon(Icons.close),
            ),
            AppHorizontalSeparator.fromAppSize(appSize: AppSize.m)
          ],
        ),
        body: SizedBox(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Padding(
            padding: EdgeInsets.all(AppSize.m.value),
            child: Column(
              children: [
                Expanded(
                  child: Center(
                    child: AspectRatio(
                      aspectRatio: 1,
                      child: PhotoWidget(
                        url: photo.url.toString(),
                      ),
                    ),
                  ),
                ),
                AppVerticalSeparator.fromAppSize(appSize: AppSize.m),
                AppButton.title(
                  title: 'ship it 🚀',
                  isLoading: false,
                  onTapUp: () => context.pop(photo),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
