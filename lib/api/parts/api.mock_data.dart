part of '../app.api.dart';

final _photoReponses = [
  'https://images.unsplash.com/photo-1714858283731-9ac8c8cba997?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw4Nnx8fGVufDB8fHx8fA%3D%3D',
  'https://plus.unsplash.com/premium_photo-1668198395277-de6e6f748065?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHwxNTB8fHxlbnwwfHx8fHw%3D',
  'https://images.unsplash.com/photo-1714675520565-5cab94e5046b?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHwxODd8fHxlbnwwfHx8fHw%3D',
  'https://images.unsplash.com/photo-1714745455353-f47a2e2b5647?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHwxOTl8fHxlbnwwfHx8fHw%3D',
  'https://images.unsplash.com/photo-1714053180988-4f725e646164?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHwyMjl8fHxlbnwwfHx8fHw%3D',
  'https://images.unsplash.com/photo-1714383625340-893175947b32?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHwyNDh8fHxlbnwwfHx8fHw%3D',
  'https://plus.unsplash.com/premium_photo-1694618623649-51733e6001fc?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHwyNjZ8fHxlbnwwfHx8fHw%3D',
  'https://images.unsplash.com/photo-1714612673675-7561c122c14e?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHwyNzJ8fHxlbnwwfHx8fHw%3D',
  'https://images.unsplash.com/photo-1713528197472-7b7f7dbb5bb4?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHwyODB8fHxlbnwwfHx8fHw%3D',
  'https://images.unsplash.com/photo-1714163399590-12e5d8c728f2?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHwyOTV8fHxlbnwwfHx8fHw%3D',
  'https://images.unsplash.com/photo-1714385998351-341d070aa79e?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHwzNDN8fHxlbnwwfHx8fHw%3D'
]
    .mapIndexed(
      (index, url) => PhotoResponse(
        id: index.toString(),
        contentType: 'image/jpg',
        url: url,
      ),
    )
    .toList();

final _chatPhotoReponses = [
  'https://images.unsplash.com/photo-1711881027254-e9ff3234d9d8?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHwzNjh8fHxlbnwwfHx8fHw%3D',
  'https://images.unsplash.com/photo-1711656166064-1b18600c26fd?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHwzNzB8fHxlbnwwfHx8fHw%3D',
  'https://images.unsplash.com/photo-1714386002041-06e84d3633f7?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHwzODB8fHxlbnwwfHx8fHw%3D',
  'https://images.unsplash.com/photo-1713498543272-128622b448f6?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw0MDN8fHxlbnwwfHx8fHw%3D',
  'https://images.unsplash.com/photo-1713204767650-8be36bc71040?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw0MTZ8fHxlbnwwfHx8fHw%3D',
  'https://images.unsplash.com/photo-1714287688144-b2406ffef8d4?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw0MzF8fHxlbnwwfHx8fHw%3D',
  'https://images.unsplash.com/photo-1713868779710-a3891cdef3a2?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw0Mzh8fHxlbnwwfHx8fHw%3D',
  'https://images.unsplash.com/photo-1714234073569-50c68b5feb4f?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw0NjV8fHxlbnwwfHx8fHw%3D',
  'https://images.unsplash.com/photo-1714151676782-3d37e0847c25?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw0OTh8fHxlbnwwfHx8fHw%3D',
  'https://images.unsplash.com/photo-1714164801285-1a24c514da35?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw1MTF8fHxlbnwwfHx8fHw%3D',
  'https://images.unsplash.com/photo-1713145872432-430c4430ec9c?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw1MjJ8fHxlbnwwfHx8fHw%3D'
]
    .mapIndexed(
      (index, url) => PhotoResponse(
        id: index.toString(),
        contentType: 'image/jpg',
        url: url,
      ),
    )
    .toList();

final _currentUser = UserReponse(
  id: '0',
  fullName: 'Joe',
  avatarUrl:
      'https://images.unsplash.com/flagged/photo-1595514191830-3e96a518989b?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Nnx8eW91bmclMjBtYW4lMjBoZWFkc2hvdHxlbnwwfHwwfHx8MA%3D%3D',
);

final _maleUserReponses = {
  'Jack':
      'https://images.unsplash.com/photo-1566753323558-f4e0952af115?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTh8fHlvdW5nJTIwbWFuJTIwaGVhZHNob3R8ZW58MHx8MHx8fDA%3D',
  'James':
      'https://images.unsplash.com/photo-1492288991661-058aa541ff43?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MjB8fHlvdW5nJTIwbWFuJTIwaGVhZHNob3R8ZW58MHx8MHx8fDA%3D',
  'Jamie':
      'https://images.unsplash.com/photo-1566492031773-4f4e44671857?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTl8fHlvdW5nJTIwbWFuJTIwaGVhZHNob3R8ZW58MHx8MHx8fDA%3D',
  'John':
      'https://plus.unsplash.com/premium_photo-1669688174645-2df488f07069?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8NXx8bWFsZXxlbnwwfHwwfHx8MA%3D%3D',
  'Jimmy':
      'https://images.unsplash.com/photo-1507003211169-0a1dd7228f2d?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MjB8fG1hbGV8ZW58MHx8MHx8fDA%3D',
}
    .mapIndexed((index, key, value) => UserReponse(
          id: 'm_$index',
          fullName: key,
          avatarUrl: value,
        ))
    .toList();

final _femaleResponse = {
  'Jessie':
      'https://plus.unsplash.com/premium_photo-1689551670902-19b441a6afde?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTN8fHdvbWFufGVufDB8fDB8fHww',
  'Jane':
      'https://images.unsplash.com/photo-1487412720507-e7ab37603c6f?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8N3x8d29tZW58ZW58MHx8MHx8fDA%3D',
  'Jessica':
      'https://images.unsplash.com/photo-1588516903720-8ceb67f9ef84?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MjB8fHdvbWVufGVufDB8fDB8fHww',
  'Josie':
      'https://images.unsplash.com/photo-1481214110143-ed630356e1bb?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8OHx8d29tZW58ZW58MHx8MHx8fDA%3D',
  'Jennifer':
      'https://images.unsplash.com/photo-1554151228-14d9def656e4?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8d29tZW4lMjBmYWNlfGVufDB8fDB8fHww',
  'Jenna':
      'https://images.unsplash.com/photo-1614436086835-d18683eb24f8?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTZ8fHdvbWVuJTIwZmFjZXxlbnwwfHwwfHx8MA%3D%3D',
}
    .mapIndexed((index, key, value) => UserReponse(
          id: 'f_$index',
          fullName: key,
          avatarUrl: value,
        ))
    .toList();
final _userReponses = [
  ..._maleUserReponses,
  ..._femaleResponse,
];
final _messageResponse = [
  "Hey! How's it going?",
  "Just wanted to say hi!",
  "Hope you're having a fantastic day!",
  "Sending positive vibes your way!",
  "You're awesome!",
  "You've got this!",
  "Thinking of you!",
  "Wishing you all the best!",
  "Remember to take a break and relax!",
  "Just wanted to brighten your day!",
  "You're doing great!",
  "Keep shining!",
  "Just dropping by to spread some cheer!",
  "Hope your day is as amazing as you are!",
  "You're making a difference!",
  "You're unstoppable!",
  "You deserve all the happiness in the world!",
  "Stay fabulous!",
  "You're one of a kind!",
  "Never forget how amazing you are!",
  "What's up, buttercup?",
  "Just wanted to say hi and brighten your day!",
  "Hey, stranger! How have you been?",
  "Hope your day is filled with smiles!",
  "Sending you a virtual hug!",
  "You're incredible, just thought you should know!",
  "You're a star in someone's sky!",
  "Remember to take time for yourself today!",
  "Life's too short to be anything but happy!",
  "You're making waves!",
  "Hope you're feeling as amazing as you are!",
  "You're a rockstar!",
  "Your vibe attracts your tribe!",
  "Today is a good day for a good day!",
  "Keep slaying!",
  "You're on fire!",
  "You're a ray of sunshine!",
  "You're the bee's knees!",
  "You're doing a great job, keep it up!",
  "Stay awesome!",
  "You're the best!"
];
