import 'package:flutter/material.dart';
import 'package:twemoji/twemoji.dart';

extension BuildContextX on BuildContext {
  void showMessage(String message) =>
      ScaffoldMessenger.of(this).showSnackBar(SnackBar(
        content: TwemojiText(text: message),
      ));
}
