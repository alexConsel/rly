import 'package:go_router/go_router.dart';
import 'package:rly/app/app_route.dart';
import 'package:rly/screens/dashboard/dashboard.page.dart';

class DashboardRoute extends AppRoute {
  @override
  String get path => '/';

  @override
  GoRoute get route => GoRoute(
        path: path,
        builder: (context, state) => const DashboardPage(),
      );
}
