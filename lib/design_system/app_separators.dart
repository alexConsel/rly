import 'package:rly/design_system/app_size.dart';
import 'package:flutter/material.dart';

class AppVerticalSeparator extends StatelessWidget {
  final AppSize appSize;

  const AppVerticalSeparator._({required this.appSize});

  factory AppVerticalSeparator.fromAppSize({required AppSize appSize}) =>
      AppVerticalSeparator._(appSize: appSize);

  @override
  Widget build(BuildContext context) {
    return SizedBox(height: appSize.value);
  }
}

class AppHorizontalSeparator extends StatelessWidget {
  final AppSize appSize;

  const AppHorizontalSeparator._({required this.appSize});

  factory AppHorizontalSeparator.fromAppSize({required AppSize appSize}) =>
      AppHorizontalSeparator._(appSize: appSize);

  @override
  Widget build(BuildContext context) {
    return SizedBox(width: appSize.value);
  }
}
