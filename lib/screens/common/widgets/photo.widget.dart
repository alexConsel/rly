import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:rly/design_system/app_size.dart';
import 'package:shimmer/shimmer.dart';

final _defaultBorderRadius = BorderRadius.circular(AppSize.m.value);

class PhotoWidget extends StatelessWidget {
  final String? url;
  final BorderRadiusGeometry? borderRadius;
  final double? width;
  final double? height;

  const PhotoWidget(
      {this.url = "https://via.placeholder.com/200x150",
      this.borderRadius,
      this.width,
      this.height,
      super.key});

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: borderRadius ?? _defaultBorderRadius,
      child: SizedBox(
        width: width,
        height: height,
        child: CachedNetworkImage(
          imageUrl: url!,
          imageBuilder: (context, imageProvider) => Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: imageProvider,
                fit: BoxFit.cover,
              ),
            ),
          ),
          placeholder: (context, url) => Shimmer.fromColors(
            baseColor: Colors.grey.shade300,
            highlightColor: Colors.grey.shade100,
            child: Container(),
          ),
          errorWidget: (context, url, error) => Container(
              color: Colors.grey.shade300, child: const Icon(Icons.error)),
        ),
      ),
    );
  }
}
