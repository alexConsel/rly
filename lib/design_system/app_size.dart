enum AppSize {
  xxs(2),
  sxs(6),
  xs(4),
  s(8),
  sm(12),
  m(16),
  ml(24),
  l(32),
  lxl(48),
  xl(64),
  xxl(128),
  xxxl(192),
  xxxxl(256),
  xxxxxl(512);

  const AppSize(this.value);

  final double value;
}
