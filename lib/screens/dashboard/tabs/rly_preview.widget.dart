part of '../dashboard.page.dart';

class RLYPreviewWidget extends StatelessWidget {
  final int notificationCount;
  final bool showNotification;
  final String url;
  final User? author;
  final TextMessage? overlayTextMessage;
  final void Function() onTapUp;

  const RLYPreviewWidget({
    required this.onTapUp,
    required this.url,
    this.overlayTextMessage,
    this.showNotification = false,
    this.notificationCount = 0,
    this.author,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return AppButton.builder(
      isLoading: false,
      onTapUp: onTapUp,
      child: Stack(
        children: [
          PhotoWidget(url: url),
          if (overlayTextMessage != null)
            Positioned.fill(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  color: Colors.black.withOpacity(0.75),
                  padding: EdgeInsets.all(AppSize.xs.value),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      SizedBox(
                        width: AppSize.xxl.value,
                        child: Text(
                          textAlign: TextAlign.end,
                          overlayTextMessage!.text,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      const Text('  •  '),
                      Opacity(
                        opacity: 0.5,
                        child: Text(
                          timeago.format(
                            DateTime.fromMillisecondsSinceEpoch(
                              overlayTextMessage!.createdAt!,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          if (notificationCount > 0 || showNotification)
            Positioned.fill(
                child: Align(
              alignment: Alignment.topRight,
              child: Transform.translate(
                offset: Offset(AppSize.sm.value, -AppSize.sm.value),
                child: NotificationBadgeWidget(
                  count: notificationCount,
                  size: AppSize.l,
                ),
              ),
            )),
          if (author != null) ...[
            Positioned.fill(
              child: Padding(
                padding: EdgeInsets.symmetric(
                    vertical: AppSize.xs.value, horizontal: AppSize.s.value),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Container(
                    decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor.withOpacity(0.5),
                        borderRadius: BorderRadius.circular(AppSize.s.value)),
                    padding: EdgeInsets.all(AppSize.xs.value),
                    child: Text(
                      author!.firstName ?? '',
                      style: const TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ),
            ),
            Positioned.fill(
              child: Align(
                alignment: Alignment.bottomLeft,
                child: Transform.translate(
                  offset: Offset(-AppSize.sm.value, AppSize.sm.value),
                  child: PhotoWidget(
                    url: author!.imageUrl,
                    width: AppSize.lxl.value,
                    height: AppSize.lxl.value,
                    borderRadius: BorderRadius.circular(AppSize.lxl.value / 2),
                  ),
                ),
              ),
            )
          ],
        ],
      ),
    );
  }
}
