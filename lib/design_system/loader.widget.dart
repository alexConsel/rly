import 'package:rly/design_system/app_size.dart';
import 'package:flutter/material.dart';

class LoaderWidget extends StatelessWidget {
  const LoaderWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        height: AppSize.m.value,
        width: AppSize.m.value,
        child: CircularProgressIndicator(
          strokeWidth: AppSize.xxs.value,
          color: Theme.of(context).textTheme.bodyMedium?.color,
        ),
      ),
    );
  }
}
