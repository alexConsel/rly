import 'package:rly/datasource/response/response.dart';
import 'package:rly/repository/entities/photo.entity.dart';

class PhotoResponse implements ApiResponse<Photo> {
  final String id;
  final String contentType;
  final String url;

  const PhotoResponse({
    required this.id,
    required this.contentType,
    required this.url,
  });

  @override
  Photo get toEntity =>
      Photo(id: id, contentType: contentType, url: Uri.parse(url));
}
