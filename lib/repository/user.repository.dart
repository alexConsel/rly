import 'package:flutter_chat_types/flutter_chat_types.dart';
import 'package:rly/datasource/auth.datasource.dart';
import 'package:rly/datasource/user.datasource.dart';

class UserRepository {
  UserRepository._();

  static UserRepository shared = UserRepository._();

  Future<User> getUser({required String id}) => UserDatasource.shared
      .getUser(id: id, token: AuthDataSource.shared.signedUser?.token)
      .then((userResponse) => userResponse.toEntity);

  Future<User?> getAuthUser() async =>
      // Fake logged user retrival delay
      Future.delayed(const Duration(milliseconds: 500)).then((_) =>
          AuthDataSource.shared.signedUser == null
              ? Future.value()
              : UserDatasource.shared
                  .getUser(
                      id: AuthDataSource.shared.signedUser!.userId,
                      token: AuthDataSource.shared.signedUser!.token)
                  .then((userResponse) => userResponse.toEntity));
}
