import 'package:flutter/material.dart';
import 'package:flutter_chat_types/flutter_chat_types.dart';
import 'package:rly/repository/entities/photo_chat.entity.dart';
import 'package:rly/repository/entities/photo_post.entity.dart';
import 'package:rly/screens/common/widgets/dropdown/dropdown.widget.dart';
import 'package:go_router/go_router.dart';
import 'package:rly/design_system/app_button/app_button.dart';
import 'package:rly/design_system/app_separators.dart';
import 'package:rly/design_system/app_size.dart';
import 'package:rly/screens/common/widgets/notification_badge.widget.dart';
import 'package:rly/screens/common/widgets/photo.widget.dart';
import 'package:rly/screens/photo_view/photo_view.page.dart';
import 'package:twemoji/twemoji.dart';
import 'package:timeago/timeago.dart' as timeago;

part 'conversation_preview.widget.dart';

class ConversationDropDown extends StatefulWidget {
  final PhotoPost photoPost;
  final double height;
  final User authUser;

  const ConversationDropDown(
      {required this.authUser,
      required this.photoPost,
      required this.height,
      super.key});

  @override
  State<ConversationDropDown> createState() => _ConversationDropDownState();
}

class _ConversationDropDownState extends State<ConversationDropDown> {
  late PhotoPost photoPost = widget.photoPost;

  @override
  Widget build(BuildContext context) {
    return DropDownWidget(
      appBar: AppBar(
        title: TwemojiText(
          text: 'conversations 💬',
          style: Theme.of(context).appBarTheme.titleTextStyle,
        ),
        backgroundColor: Colors.transparent,
        automaticallyImplyLeading: false,
        centerTitle: true,
        actions: [
          AppButton.builder(
            isLoading: false,
            onTapUp: () => context.pop(),
            child: const Icon(Icons.close),
          ),
          AppHorizontalSeparator.fromAppSize(appSize: AppSize.m)
        ],
      ),
      height: widget.height,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: AppSize.m.value),
        child: ListView.separated(
          itemCount: photoPost.photoChats.length + 1,
          separatorBuilder: (_, __) => const Divider(
            color: Colors.grey,
            thickness: 1,
            height: 0,
          ),
          itemBuilder: (context, index) {
            if (index == 0) {
              return SizedBox(
                height: 150,
                child: Padding(
                  padding: EdgeInsets.all(AppSize.m.value),
                  child: Center(
                    child: AppButton.builder(
                      isLoading: false,
                      onTapUp: () => showDialog(
                        context: context,
                        builder: (context) => Dialog(
                          child: PhotoViewPage(
                            photo: widget.photoPost.photo,
                          ),
                        ),
                      ),
                      child: AspectRatio(
                          aspectRatio: 1,
                          child: PhotoWidget(
                            url: photoPost.photo.url.toString(),
                          )),
                    ),
                  ),
                ),
              );
            } else {
              final chat = photoPost.photoChats[index - 1];
              return AppButton.builder(
                animateScale: false,
                isLoading: false,
                onTapUp: () => context.pop(chat),
                child: _ConversationPreviewWidget(
                  chat: chat,
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
