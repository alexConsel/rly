import 'package:flutter_chat_types/flutter_chat_types.dart';
import 'package:rly/datasource/response/response.dart';

class UserReponse implements ApiResponse<User> {
  final String id;
  final String fullName;
  final String avatarUrl;

  UserReponse(
      {required this.id, required this.fullName, required this.avatarUrl});

  @override
  User get toEntity => User(id: id, firstName: fullName, imageUrl: avatarUrl);
}
