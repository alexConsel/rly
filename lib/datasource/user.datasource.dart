import 'package:rly/api/app.api.dart';
import 'package:rly/datasource/response/user.response.dart';

class UserDatasource {
  static UserDatasource shared = UserDatasource._();

  UserDatasource._();

  Future<UserReponse> getUser({required String id, required String? token}) =>
      AppApi.shared.getUser(id: id, token: token);
}
