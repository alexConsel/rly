part of './app_button.dart';

class AppButtonContent extends StatefulWidget {
  final bool isLoading;
  final bool showLoader;
  final bool enabled;
  final Widget child;
  final Animation<double> animation;
  final bool isCustom;
  final bool animated;
  final bool animateScale;

  const AppButtonContent({
    required this.animation,
    required this.isLoading,
    required this.showLoader,
    required this.animated,
    required this.animateScale,
    required this.enabled,
    required this.child,
    this.isCustom = false,
    super.key,
  });

  @override
  State<AppButtonContent> createState() => AppButtonContentState();
}

class AppButtonContentState extends State<AppButtonContent> {
  bool get showLoader => widget.isLoading && widget.showLoader;
  bool get isFaded => widget.isLoading || !widget.enabled;

  Widget get baseChild => AnimatedBuilder(
        animation: widget.animation,
        builder: (context, child) {
          return Opacity(
            opacity: 1 - widget.animation.value * 0.25,
            child: child,
          );
        },
        child: Stack(
          children: [
            Visibility.maintain(
              visible: !showLoader,
              child: isFaded
                  ? Opacity(
                      opacity: 0.5,
                      child: widget.child,
                    )
                  : widget.child,
            ),
            if (showLoader) const Positioned.fill(child: LoaderWidget())
          ],
        ),
      );

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: widget.animation,
      builder: (context, child) {
        return Transform.scale(
          scale: widget.animateScale ? 1 - 0.05 * widget.animation.value : 1,
          child: child,
        );
      },
      child: widget.isCustom
          ? baseChild
          : LayoutBuilder(builder: (context, constraints) {
              return Container(
                padding: EdgeInsets.symmetric(
                  vertical: AppSize.m.value,
                  horizontal: AppSize.l.value,
                ),
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.circular(AppSize.m.value),
                  // boxShadow: [
                  //   BoxShadow(
                  //     color: Colors.grey.withOpacity(0.2),
                  //     spreadRadius: 5,
                  //     blurRadius: 7,
                  //     offset: const Offset(0, 0),
                  //   ),
                  // ]
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(AppSize.m.value),
                  child: baseChild,
                ),
              );
            }),
    );
  }
}
