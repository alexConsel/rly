import 'package:rly/datasource/unsplash.datasource.dart';
import 'package:rly/repository/entities/photo.entity.dart';

class PhotoRepository {
  PhotoRepository._();

  static PhotoRepository shared = PhotoRepository._();

  Future<List<Photo>> getUserPhotos() =>
      UnsplashDataSource.shared.getFeaturedPhotos().then(
            (images) => images.map((image) => image.toEntity).toList(),
          );
}
