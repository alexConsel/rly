import 'package:flutter/material.dart';
import 'package:rly/design_system/app_size.dart';

class DropDownWidget extends StatelessWidget {
  final double height;
  final AppBar? appBar;
  final Widget child;

  const DropDownWidget(
      {required this.height, required this.child, this.appBar, super.key});

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(AppSize.m.value),
          topRight: Radius.circular(AppSize.m.value)),
      child: SizedBox(
        height: height,
        child: Scaffold(
          body: child,
          appBar: appBar,
        ),
      ),
    );
  }
}
