import 'package:flutter_chat_types/flutter_chat_types.dart';

import 'package:rly/repository/entities/photo.entity.dart';

class PhotoChat {
  final Photo photo;
  final String id;
  final DateTime createdAt;
  final DateTime updatedAt;
  final User user;
  final List<Message> chatMessages;

  final bool hasUnreadMessages;

  const PhotoChat({
    required this.photo,
    required this.id,
    required this.createdAt,
    required this.updatedAt,
    required this.user,
    required this.chatMessages,
    required this.hasUnreadMessages,
  });

  Message? get lastMessage => chatMessages.firstOrNull;

  TextMessage? get lastTextMessage => switch (lastMessage) {
        TextMessage() => lastMessage as TextMessage?,
        _ => null,
      };

  PhotoChat copyWith({
    Photo? photo,
    String? id,
    DateTime? createdAt,
    DateTime? updatedAt,
    User? user,
    List<Message>? chatMessages,
    bool? hasUnreadMessages,
  }) {
    return PhotoChat(
      photo: photo ?? this.photo,
      id: id ?? this.id,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
      user: user ?? this.user,
      chatMessages: chatMessages ?? this.chatMessages,
      hasUnreadMessages: hasUnreadMessages ?? this.hasUnreadMessages,
    );
  }
}
