import 'package:collection/collection.dart';
import 'package:flutter_chat_types/flutter_chat_types.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:rly/repository/entities/photo_chat.entity.dart';
import 'package:rly/repository/helper/list.extension.dart';
import 'package:rly/repository/photo_chat.repository.dart';

//TODO: these two providers are kinda similar - could be factorized
class AsyncPhotoChatProvider extends AsyncNotifier<List<PhotoChat>> {
  int _page = 0;
  bool _hasMoreContent = true;

  bool get hasMoreContent => _hasMoreContent;

  @override
  Future<List<PhotoChat>> build() async => [];

  Future<void> fetchMorePhotoChats() async {
    if (!_hasMoreContent) return;

    final previousState = await future;

    state = const AsyncValue.loading();

    PhotoChatRepository.shared.getPhotoChats(page: _page).then((newPhotoChats) {
      newPhotoChats.isNotEmpty ? _page += 1 : _hasMoreContent = false;
      state =
          AsyncData((previousState..addAll(newPhotoChats)).sorted(_compare));
    }).onError((error, stackTrace) => throw Exception('error while loading'));
  }

  Future<void> markPhotoChatAsRead({required PhotoChat photoChat}) async {
    final previousState = await future;

    PhotoChatRepository.shared
        .readPhotoChat(photoChatId: photoChat.id)
        .then((_) {
      _updatePhotoChat(
          newPhotoChat: photoChat.copyWith(hasUnreadMessages: false),
          previousState: previousState);
    }).onError((error, stackTrace) {
      //No need to notify the user about this
    });
  }

  Future<void> addMessage(
      {required PhotoChat photoChat,
      required User author,
      required String text}) async {
    final previousState = await future;

    state = const AsyncValue.loading();

    PhotoChatRepository.shared.addTextMessage(message: text).then((_) {
      final now = DateTime.now();
      final timestamp = now.millisecondsSinceEpoch;
      final newPhotoChat = photoChat.copyWith(
          updatedAt: now,
          chatMessages: photoChat.chatMessages
            ..insert(
              0,
              TextMessage(
                  id: timestamp.toString(),
                  author: author,
                  createdAt: timestamp,
                  text: text),
            ));
      _updatePhotoChat(
        newPhotoChat: newPhotoChat,
        previousState: previousState,
      );
    }).onError((_, __) => throw Exception('error while sending message'));
  }

  void _updatePhotoChat(
      {required PhotoChat newPhotoChat,
      required List<PhotoChat> previousState}) {
    state = AsyncData(previousState
        .replacedWhere(
            (photoChat) => photoChat.id == newPhotoChat.id, newPhotoChat)
        .sorted(_compare));
  }

  int _compare(PhotoChat chatA, PhotoChat chatB) =>
      chatB.updatedAt.compareTo(chatA.updatedAt);
}

final asyncPhotoChatProvider =
    AsyncNotifierProvider<AsyncPhotoChatProvider, List<PhotoChat>>(() {
  return AsyncPhotoChatProvider();
});
