import 'package:rly/design_system/app_size.dart';
import 'package:rly/design_system/app_sizing.dart';
import 'package:rly/design_system/loader.widget.dart';
import 'package:flutter/material.dart';
import 'package:twemoji/twemoji.dart';

part './app_button_content.dart';

class AppButton extends StatefulWidget {
  final String? title;
  final Widget? child;
  final AppSizing sizing;
  final bool isLoading;
  final bool showLoader;
  final bool animated;
  final bool isCustom;
  final bool enabled;
  final bool animateScale;
  final Function()? onTapUp;

  factory AppButton.builder({
    required bool isLoading,
    required Function()? onTapUp,
    required Widget child,
    bool enabled = true,
    bool animated = false,
    bool animateScale = true,
  }) =>
      AppButton._(
        title: null,
        isLoading: isLoading,
        onTapUp: onTapUp,
        isCustom: true,
        animated: animated,
        showLoader: false,
        enabled: enabled,
        animateScale: animateScale,
        child: child,
      );

  factory AppButton.title({
    required String title,
    required bool isLoading,
    required Function()? onTapUp,
    bool enabled = true,
    bool animated = false,
    bool animateScale = true,
  }) =>
      AppButton._(
        title: title,
        child: null,
        isLoading: isLoading,
        isCustom: false,
        animated: animated,
        enabled: enabled,
        animateScale: animateScale,
        onTapUp: onTapUp,
      );

  const AppButton._({
    required this.title,
    required this.child,
    required this.isLoading,
    required this.onTapUp,
    required this.isCustom,
    required this.enabled,
    required this.animateScale,
    this.showLoader = true,
    this.animated = true,
    super.key,
    this.sizing = AppSizing.m,
  });

  @override
  State<AppButton> createState() => AppButtonState();
}

class AppButtonState extends State<AppButton>
    with SingleTickerProviderStateMixin {
  late final Animation<double> _animation =
      Tween<double>(begin: 0.0, end: 1.0).animate(_controller);
  late final AnimationController _controller = AnimationController(
      duration: const Duration(milliseconds: 100), vsync: this);

  late bool isAnimationPaused = false;

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      ignoring: widget.isLoading || !widget.enabled,
      child: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTapDown: widget.onTapUp == null
              ? null
              : (_) {
                  _controller.forward();
                  setState(() {
                    isAnimationPaused = true;
                  });
                },
          onTapCancel: widget.onTapUp == null
              ? null
              : () async {
                  await _controller.reverse();
                  setState(() {
                    isAnimationPaused = false;
                  });
                },
          onTapUp: widget.onTapUp == null
              ? null
              : (_) async {
                  widget.onTapUp!();
                  if (!_animation.isCompleted) await _controller.forward();
                  await _controller.reverse();
                  setState(() {
                    isAnimationPaused = false;
                  });
                },
          child: AppButtonContent(
            animation: _animation,
            isLoading: widget.isLoading,
            showLoader: widget.showLoader,
            isCustom: widget.isCustom,
            enabled: widget.enabled,
            animateScale: widget.animateScale,
            animated:
                widget.animated && !isAnimationPaused && !widget.isLoading,
            child: widget.title == null
                ? widget.child == null
                    ? const SizedBox.shrink()
                    : widget.child!
                : TwemojiText(
                    text: widget.title!,
                    style: Theme.of(context)
                        .textTheme
                        .titleMedium
                        ?.apply(fontSizeFactor: 1.1)
                        .copyWith(fontWeight: FontWeight.w600),
                  ),
          )),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}
