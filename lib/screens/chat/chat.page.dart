import 'package:flutter/material.dart';
import 'package:flutter_chat_ui/flutter_chat_ui.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:rly/design_system/app_button/app_button.dart';
import 'package:rly/design_system/app_separators.dart';
import 'package:rly/design_system/app_size.dart';
import 'package:rly/repository/entities/photo_chat.entity.dart';
import 'package:rly/screens/chat/chat.route.dart';
import 'package:rly/screens/common/widgets/photo.widget.dart';
import 'package:rly/screens/error/error.page.dart';
import 'package:rly/screens/helpers/context.extension.dart';
import 'package:rly/screens/loading/loading.page.dart';
import 'package:rly/screens/photo_view/photo_view.page.dart';
import 'package:rly/screens/providers/photo_chat.provider.dart';
import 'package:rly/screens/providers/photo_post.provider.dart';
import 'package:twemoji/twemoji.dart';
import 'package:collection/collection.dart';

class ChatPage extends ConsumerStatefulWidget {
  final ChatPageParam pageParam;

  const ChatPage({required this.pageParam, super.key});

  @override
  ConsumerState<ChatPage> createState() => _ChatPageState();
}

class _ChatPageState extends ConsumerState<ChatPage> {
  @override
  Widget build(BuildContext context) {
    final avatarSize = AppSize.l.value;

    final asyncPhotoChat = switch (widget.pageParam) {
      PhotoChatParam(:final chatId) => ref.watch(
          asyncPhotoChatProvider.select(
            (asyncPhotoChats) => asyncPhotoChats.whenData(
              (photoChats) => photoChats
                  .firstWhereOrNull((photoChat) => photoChat.id == chatId),
            ),
          ),
        ),
      PhotoPostParam(:final postId, :final chatId) => ref.watch(
          asyncPhotoPostProvider.select(
            (asyncPhotoPosts) => asyncPhotoPosts.whenData(
              (photoPosts) => photoPosts
                  .firstWhereOrNull((photoPost) => photoPost.id == postId)
                  ?.photoChats
                  .firstWhereOrNull((photoChat) => photoChat.id == chatId),
            ),
          ),
        ),
    };

    final messages = asyncPhotoChat.value?.chatMessages ?? [];

    return Scaffold(
      appBar: AppBar(
        title: TwemojiText(
          text: asyncPhotoChat.value?.user.firstName ?? 'Chat',
          style: Theme.of(context).appBarTheme.titleTextStyle,
        ),
        actions: [
          AppButton.builder(
            isLoading: false,
            onTapUp: () => showDialog(
              context: context,
              builder: (context) => Dialog(
                child: PhotoViewPage(
                  photo: widget.pageParam.photo,
                ),
              ),
            ),
            child: PhotoWidget(
              url: widget.pageParam.photo.url.toString(),
              borderRadius: BorderRadius.circular(AppSize.xs.value),
              width: AppSize.l.value,
              height: AppSize.l.value,
            ),
          ),
          AppHorizontalSeparator.fromAppSize(appSize: AppSize.m)
        ],
      ),
      body: switch (asyncPhotoChat) {
        AsyncData<PhotoChat?>() ||
        AsyncLoading() =>
          messages.isEmpty && asyncPhotoChat is AsyncLoading
              ? const LoadingPage()
              : Chat(
                  theme: DarkChatTheme(
                    primaryColor: Theme.of(context).primaryColor,
                    backgroundColor: Theme.of(context).scaffoldBackgroundColor,
                    inputBackgroundColor:
                        Theme.of(context).appBarTheme.backgroundColor ??
                            Colors.black,
                  ),
                  showUserAvatars: true,
                  avatarBuilder: (author) => PhotoWidget(
                    url: author.imageUrl,
                    width: avatarSize,
                    height: avatarSize,
                    borderRadius: BorderRadius.circular(avatarSize / 2),
                  ),
                  messages: messages,
                  onSendPressed: (partialText) =>
                      _addMessage(partialText.text, asyncPhotoChat.value),
                  user: widget.pageParam.authUser,
                ),
        _ => const ErrorPage(errorMessage: 'error...')
      },
    );
  }

  void _addMessage(String text, PhotoChat? photoChat) => photoChat == null
      ? null
      : switch (widget.pageParam) {
          PhotoChatParam() => ref
              .read(asyncPhotoChatProvider.notifier)
              .addMessage(
                  photoChat: photoChat,
                  author: widget.pageParam.authUser,
                  text: text)
              .onError(
                (error, stackTrace) => context.showMessage(error.toString()),
              ),
          PhotoPostParam(:final postId) => ref
              .read(asyncPhotoPostProvider.notifier)
              .addMessage(
                  photoPostId: postId,
                  photoChat: photoChat,
                  author: widget.pageParam.authUser,
                  text: text)
              .onError(
                (error, stackTrace) => context.showMessage(
                  error.toString(),
                ),
              ),
        };
}
