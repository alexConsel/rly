import 'package:rly/datasource/auth.datasource.dart';
import 'package:rly/datasource/photo_post.datasource.dart';
import 'package:rly/datasource/request_param/create_photo_chat.request_param.dart';
import 'package:rly/repository/entities/photo.entity.dart';
import 'package:rly/repository/entities/photo_post.entity.dart';

class PhotoPostRepository {
  PhotoPostRepository._();

  static PhotoPostRepository shared = PhotoPostRepository._();

  Future<List<PhotoPost>> getPhotoPosts({required int page}) =>
      PhotoPostDataSource.shared
          .getPhotoPosts(
              page: page, token: AuthDataSource.shared.signedUser?.token)
          .then(
            (photoPostResponses) => photoPostResponses
                .map((photoPostResponse) => photoPostResponse.toEntity)
                .toList(),
          );

  Future<PhotoPost> createPhotoPost({required Photo photo}) =>
      PhotoPostDataSource.shared
          .createPhotoPost(
              param:
                  CreatePhotoChatRequestParam(photoUrl: photo.url.toString()),
              token: AuthDataSource.shared.signedUser?.token)
          .then(
            (postResponse) => postResponse.toEntity,
          );
}
