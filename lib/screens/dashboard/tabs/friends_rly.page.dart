part of '../dashboard.page.dart';

//TODO: this could be factorized with MyRLYPage
class FriendsRLYPage extends ConsumerStatefulWidget {
  final User authUser;

  const FriendsRLYPage({required this.authUser, super.key});

  @override
  ConsumerState<FriendsRLYPage> createState() => _FriendsRLYPageState();
}

class _FriendsRLYPageState extends ConsumerState<FriendsRLYPage>
    with AutomaticKeepAliveClientMixin<FriendsRLYPage> {
  late final _notifier = ref.read(asyncPhotoChatProvider.notifier);

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_) {
      _fetchMoreChats();
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    final asyncPhotoChat = ref.watch(asyncPhotoChatProvider);

    final itemCount = (asyncPhotoChat.value?.length ?? 0) + 1;
    return Padding(
      padding: EdgeInsets.all(AppSize.l.value),
      child: switch (asyncPhotoChat) {
        AsyncData<List<PhotoChat>>() || AsyncLoading() => ListView.separated(
            clipBehavior: Clip.none,
            itemCount: itemCount,
            itemBuilder: (BuildContext context, int index) {
              if (index == itemCount - 1) {
                return _notifier.hasMoreContent
                    ? VisibilityDetector(
                        key: const Key('fr_rly_loader'),
                        child: SizedBox(
                            height: AppSize.xl.value,
                            child: const LoaderWidget()),
                        onVisibilityChanged: (_) {
                          _fetchMoreChats();
                        },
                      )
                    : const SizedBox.shrink();
              }

              final photoChat = asyncPhotoChat.value![index];
              return SizedBox(
                height: AppSize.xxl.value,
                child: Column(
                  children: [
                    Expanded(
                      child: RLYPreviewWidget(
                        showNotification: photoChat.hasUnreadMessages,
                        onTapUp: () {
                          _markChatAsRead(photoChat);
                          _goToChat(photoChat);
                        },
                        author: photoChat.user,
                        url: photoChat.photo.url.toString(),
                        overlayTextMessage: photoChat.lastTextMessage,
                      ),
                    ),
                    AppVerticalSeparator.fromAppSize(appSize: AppSize.l)
                  ],
                ),
              );
            },
            separatorBuilder: (_, __) => const Divider(
              color: Colors.grey,
              thickness: 1,
              height: 0,
            ),
          ),
        _ => ErrorPage(
            errorMessage: 'error...',
            onReload: () => ref.invalidate(asyncPhotoPostProvider),
          )
      },
    );
  }

  void _fetchMoreChats() => _notifier.hasMoreContent
      ? _notifier
          .fetchMorePhotoChats()
          .onError((error, stackTrace) => context.showMessage(error.toString()))
      : null;

  void _markChatAsRead(PhotoChat photoChat) => _notifier
          .markPhotoChatAsRead(photoChat: photoChat)
          .onError((error, stackTrace) {
        //do nothing
      });

  void _goToChat(PhotoChat photoChat) => context.push<PhotoChat>(
        ChatRoute().path,
        extra: PhotoChatParam(
          photo: photoChat.photo,
          chatId: photoChat.id,
          authUser: widget.authUser,
        ),
      );

  @override
  bool get wantKeepAlive => true;
}
