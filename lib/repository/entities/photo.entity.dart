class Photo {
  final String id;
  final String contentType;
  final Uri url;

  const Photo({
    required this.id,
    required this.contentType,
    required this.url,
  });
}
