part of '../app.api.dart';

DateTime _generateTimeStamp() {
  DateTime now = DateTime.now();
  DateTime threeMonthsAgo = DateTime(now.year, now.month - 2, 1);
  ;
  DateTime endOfLastMonth = DateTime(now.year, now.month, 0);
  return threeMonthsAgo.add(Duration(
      days:
          Random().nextInt(endOfLastMonth.difference(threeMonthsAgo).inDays)));
}

List<PhotoChatResponse> _generatePhotoChatsFromPhotos() {
  final List<PhotoChatResponse> result = [];

  final photos = [..._chatPhotoReponses];
  int count = photos.length;

  final timestamp = _generateTimeStamp();

  final users = [..._userReponses];

  while (count > 0) {
    final photo = photos.removeAt(Random().nextInt(photos.length));
    final user = (users..shuffle())[Random().nextInt(_userReponses.length)];
    result.add(
      PhotoChatResponse(
          id: count.toString(),
          createdAt: timestamp,
          updatedAt: timestamp,
          chatMessages: _generateMessages(user),
          user: user,
          photo: photo,
          hasUnreadMessages: Random().nextBool()),
    );
    count -= 1;
  }
  return result;
}

List<PhotoPostResponse> _generatePhotoPosts() {
  final List<PhotoPostResponse> result = [];

  final photos = [..._photoReponses];
  int count = photos.length;

  final timestamp = _generateTimeStamp();

  while (count > 0) {
    final photo = photos.removeAt(Random().nextInt(photos.length));
    final photoChats = _generatePhotoChats(photo);
    result.add(
      PhotoPostResponse(
          id: count.toString(),
          photoChats: photoChats,
          updatedAt: timestamp,
          photo: photo,
          unreadConversationsCount: photoChats.fold(
            0,
            (cnt, photoChat) => cnt + (photoChat.hasUnreadMessages ? 1 : 0),
          )),
    );
    count -= 1;
  }
  return result;
}

List<PhotoChatResponse> _generatePhotoChats(PhotoResponse photo) {
  final List<PhotoChatResponse> result = [];

  final timestamp = _generateTimeStamp();

  final users = [..._userReponses]..shuffle();
  int count = Random().nextInt(users.length);
  while (count > 0) {
    final user = users.removeAt(Random().nextInt(users.length));
    result.add(PhotoChatResponse(
        id: count.toString(),
        photo: photo,
        createdAt: timestamp,
        updatedAt: timestamp,
        user: user,
        chatMessages: _generateMessages(user),
        hasUnreadMessages: Random().nextBool()));
    count -= 1;
  }
  return result;
}

List<MessageResponse> _generateMessages(UserReponse user) {
  final List<MessageResponse> result = [];

  final messages = [..._messageResponse]..shuffle();
  int count = Random().nextInt(messages.length);

  final timestamp = _generateTimeStamp();

  while (count > 0) {
    result.add(
      MessageResponse(
        id: count.toString(),
        author: Random().nextBool() ? user : _currentUser,
        body: messages.removeAt(Random().nextInt(messages.length)),
        createdAt: timestamp.add(
          Duration(minutes: count),
        ),
      ),
    );
    count -= 1;
  }
  return result;
}
