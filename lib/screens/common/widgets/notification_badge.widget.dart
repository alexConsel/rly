import 'package:flutter/material.dart';
import 'package:rly/design_system/app_size.dart';

class NotificationBadgeWidget extends StatelessWidget {
  final AppSize size;
  final int? count;

  const NotificationBadgeWidget(
      {this.count, this.size = AppSize.lxl, super.key});

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(AppSize.l.value),
      child: Container(
        width: size.value,
        height: size.value,
        color: Colors.red,
        child: count == null || count == 0
            ? Container()
            : Center(
                child: Text(
                  count.toString(),
                  maxLines: 1,
                  style: TextStyle(
                      fontSize: size.value / 2.5, fontWeight: FontWeight.bold),
                ),
              ),
      ),
    );
  }
}
