import 'package:rly/datasource/response/message.response.dart';
import 'package:rly/datasource/response/photo.response.dart';
import 'package:rly/datasource/response/response.dart';
import 'package:rly/datasource/response/user.response.dart';
import 'package:rly/repository/entities/photo_chat.entity.dart';

class PhotoChatResponse implements ApiResponse<PhotoChat> {
  final PhotoResponse photo;
  final String id;
  final DateTime createdAt;
  final DateTime updatedAt;
  final UserReponse user;
  final List<MessageResponse> chatMessages;

  final bool hasUnreadMessages;

  const PhotoChatResponse({
    required this.id,
    required this.photo,
    required this.createdAt,
    required this.updatedAt,
    required this.user,
    required this.chatMessages,
    required this.hasUnreadMessages,
  });

  @override
  PhotoChat get toEntity => PhotoChat(
        id: id,
        photo: photo.toEntity,
        createdAt: createdAt,
        updatedAt: updatedAt,
        user: user.toEntity,
        chatMessages: chatMessages.map((message) => message.toEntity).toList(),
        hasUnreadMessages: hasUnreadMessages,
      );
}
