import 'package:rly/api/app.api.dart';
import 'package:rly/datasource/response/photo_chat.response.dart';

class PhotoChatDataSource {
  static PhotoChatDataSource shared = PhotoChatDataSource._();
  PhotoChatDataSource._();

  Future<List<PhotoChatResponse>> getPhotoChats(
          {required int page, required String? token}) =>
      AppApi.shared.getPhotoChats(page: page, token: token);

  Future<void> addTextMessage(
          {required String message, required String? token}) =>
      AppApi.shared.addTextMessage(message: message, token: token);

  Future<void> readPhotoChat(
          {required String photoChatId, required String? token}) =>
      AppApi.shared.addTextMessage(message: photoChatId, token: token);
}
