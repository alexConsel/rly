import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:rly/screens/chat/chat.route.dart';
import 'package:rly/screens/dashboard/dashboard.route.dart';

const _kPrimaryColor = Color(0xffd60dd4);

class App extends StatefulWidget {
  const App({super.key});

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  final goRouter = GoRouter(
    initialLocation: DashboardRoute().path,
    routes: [
      DashboardRoute().route,
      ChatRoute().route,
    ],
  );

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routerConfig: goRouter,
      theme: ThemeData(
        snackBarTheme: SnackBarThemeData(backgroundColor: _kPrimaryColor),
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        brightness: Brightness.dark,
        primaryColor: _kPrimaryColor,
        tabBarTheme: const TabBarTheme(
            splashFactory: NoSplash.splashFactory,
            labelStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
            unselectedLabelStyle:
                TextStyle(fontSize: 16, fontWeight: FontWeight.w600)),
        scaffoldBackgroundColor: const Color(0xff1e041b),
        bottomNavigationBarTheme: const BottomNavigationBarThemeData().copyWith(
          backgroundColor: const Color(0xff291126),
          selectedItemColor: _kPrimaryColor,
        ),
        appBarTheme: const AppBarTheme(
          titleTextStyle: TextStyle(
            fontSize: 25,
            fontWeight: FontWeight.w900,
          ),
        ),
        fontFamily: 'Montserrat',
      ),
    );
  }
}
