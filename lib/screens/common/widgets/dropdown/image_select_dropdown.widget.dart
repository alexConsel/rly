import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:rly/design_system/app_button/app_button.dart';
import 'package:rly/design_system/app_separators.dart';
import 'package:rly/design_system/app_size.dart';
import 'package:rly/repository/photo.repository.dart';
import 'package:rly/screens/common/widgets/dropdown/dropdown.widget.dart';
import 'package:rly/screens/common/widgets/photo.widget.dart';
import 'package:rly/screens/error/error.page.dart';
import 'package:rly/screens/loading/loading.page.dart';
import 'package:twemoji/twemoji.dart';

class ImageSelectDropDownWidget extends StatefulWidget {
  final double height;

  const ImageSelectDropDownWidget({required this.height, super.key});

  @override
  State<ImageSelectDropDownWidget> createState() =>
      _ImageSelectDropDownWidgetState();
}

class _ImageSelectDropDownWidgetState extends State<ImageSelectDropDownWidget> {
  final userPhotosFuture = PhotoRepository.shared.getUserPhotos();

  @override
  Widget build(BuildContext context) {
    return DropDownWidget(
      appBar: AppBar(
        title: TwemojiText(
          text: 'photo gallery 🖼️',
          style: Theme.of(context).appBarTheme.titleTextStyle,
        ),
        backgroundColor: Colors.transparent,
        automaticallyImplyLeading: false,
        centerTitle: true,
        actions: [
          AppButton.builder(
            isLoading: false,
            onTapUp: () => context.pop(),
            child: const Icon(Icons.close),
          ),
          AppHorizontalSeparator.fromAppSize(appSize: AppSize.m)
        ],
      ),
      height: widget.height,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: AppSize.m.value),
        child: FutureBuilder(
          future: userPhotosFuture,
          builder: (context, snapshot) => switch (snapshot.connectionState) {
            ConnectionState.done => snapshot.hasData
                ? GridView.builder(
                    itemCount: snapshot.data!.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      mainAxisSpacing: AppSize.l.value,
                      crossAxisSpacing: AppSize.l.value,
                      childAspectRatio: 1,
                    ),
                    itemBuilder: (context, index) => AppButton.builder(
                      isLoading: false,
                      onTapUp: () => context.pop(snapshot.data![index]),
                      child: PhotoWidget(
                        url: snapshot.data![index].url.toString(),
                      ),
                    ),
                  )
                : const ErrorPage(
                    errorMessage: 'we could not access your gallery'),
            _ => const LoadingPage(),
          },
        ),
      ),
    );
  }
}
