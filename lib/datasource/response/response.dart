abstract class ApiResponse<T> {
  T get toEntity;
}
