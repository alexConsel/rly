import 'package:collection/collection.dart';
import 'package:flutter_chat_types/flutter_chat_types.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:rly/repository/entities/photo.entity.dart';
import 'package:rly/repository/entities/photo_chat.entity.dart';
import 'package:rly/repository/entities/photo_post.entity.dart';
import 'package:rly/repository/helper/list.extension.dart';
import 'package:rly/repository/photo_chat.repository.dart';
import 'package:rly/repository/photo_post.repository.dart';

//TODO: these two providers are kinda similar - could be factorized
class AsyncPhotoPostProvider extends AsyncNotifier<List<PhotoPost>> {
  int _page = 0;
  bool _hasMoreContent = true;

  bool get hasMoreContent => _hasMoreContent;

  @override
  Future<List<PhotoPost>> build() async => [];

  Future<void> fetchMorePhotoPosts() async {
    if (!_hasMoreContent) return;

    final previousState = await future;

    state = const AsyncValue.loading();

    PhotoPostRepository.shared.getPhotoPosts(page: _page).then((newPhotoPosts) {
      newPhotoPosts.isNotEmpty ? _page += 1 : _hasMoreContent = false;
      state =
          AsyncData((previousState..addAll(newPhotoPosts)).sorted(_compare));
    }).onError((error, stackTrace) => throw Exception('error while loading'));
  }

  Future<void> createPhotoPost({required Photo photo}) async {
    final previousState = await future;

    state = const AsyncValue.loading();

    PhotoPostRepository.shared.createPhotoPost(photo: photo).then((photoPost) {
      state = AsyncData(previousState..insert(0, photoPost));
    }).onError((_, __) => throw Exception('error while creating RLY'));
  }

  Future<void> markPhotoChatAsRead(
      {required PhotoPost photoPost, required PhotoChat photoChat}) async {
    final previousState = await future;

    PhotoChatRepository.shared
        .readPhotoChat(photoChatId: photoChat.id)
        .then((_) {
      _updatePhotoPost(
          photoPost: photoPost,
          newPhotoChat: photoChat.copyWith(hasUnreadMessages: false),
          previousState: previousState);
    }).onError((error, stackTrace) {
      //No need to notify the user about this
    });
  }

  PhotoPost? _getPhotoPost(
          {required List<PhotoPost> photoPosts, required String photoPostId}) =>
      photoPosts.firstWhereOrNull((photoPost) => photoPost.id == photoPostId);

  Future<void> addMessage(
      {required String photoPostId,
      required PhotoChat photoChat,
      required User author,
      required String text}) async {
    final previousState = await future;

    state = const AsyncValue.loading();

    final photoPost =
        _getPhotoPost(photoPosts: previousState, photoPostId: photoPostId);

    if (photoPost == null) throw Exception('error RLY could not be found');

    PhotoChatRepository.shared.addTextMessage(message: text).then((_) {
      final now = DateTime.now();
      final timestamp = now.millisecondsSinceEpoch;
      final newPhotoChat = photoChat.copyWith(
          updatedAt: now,
          chatMessages: photoChat.chatMessages
            ..insert(
              0,
              TextMessage(
                  id: timestamp.toString(),
                  author: author,
                  createdAt: timestamp,
                  text: text),
            ));

      final newPhotoPost = photoPost.copyWith(updatedAt: now);
      _updatePhotoPost(
        newPhotoChat: newPhotoChat,
        photoPost: newPhotoPost,
        previousState: previousState,
      );
    }).onError((_, __) => throw Exception('error while sending message'));
  }

  void _updatePhotoPost(
      {required PhotoPost photoPost,
      required PhotoChat newPhotoChat,
      required List<PhotoPost> previousState}) {
    final newPhotoChats = photoPost.photoChats.replacedWhere(
      (chat) => chat.id == newPhotoChat.id,
      newPhotoChat,
    );
    final newPhotoPost = photoPost.copyWith(
        photoChats: newPhotoChats,
        unreadConversationsCount: newPhotoChats.fold(
          0,
          (count, photoChat) => count! + (photoChat.hasUnreadMessages ? 1 : 0),
        ));

    state = AsyncData(previousState
        .replacedWhere(
            (photoPost) => photoPost.id == newPhotoPost.id, newPhotoPost)
        .sorted(_compare));
  }

  int _compare(PhotoPost postA, PhotoPost postB) =>
      postB.updatedAt.compareTo(postA.updatedAt);
}

final asyncPhotoPostProvider =
    AsyncNotifierProvider<AsyncPhotoPostProvider, List<PhotoPost>>(() {
  return AsyncPhotoPostProvider();
});
