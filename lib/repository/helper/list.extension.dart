extension ListX<E> on List<E> {
  List<E> replacedWhere(bool Function(E element) checkFunction, E newElement) =>
      map((element) => checkFunction(element) ? newElement : element).toList();
}
