part of '../dashboard.page.dart';

class _AddButtonWidget extends StatelessWidget {
  const _AddButtonWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(AppSize.xl.value),
      child: Container(
        height: AppSize.xl.value,
        width: AppSize.xl.value,
        color: Theme.of(context).primaryColor,
        child: Center(
          child: Twemoji(
            emoji: '📸',
            height: AppSize.l.value,
            width: AppSize.l.value,
            fit: BoxFit.fill,
          ),
        ),
      ),
    );
  }
}
