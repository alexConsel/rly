import 'package:flutter/material.dart';
import 'package:rly/design_system/app_button/app_button.dart';
import 'package:rly/design_system/app_separators.dart';
import 'package:rly/design_system/app_size.dart';

class ErrorPage extends StatelessWidget {
  final String errorMessage;
  final void Function()? onReload;

  const ErrorPage({
    required this.errorMessage,
    this.onReload,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(errorMessage),
          if (onReload != null) ...[
            AppVerticalSeparator.fromAppSize(appSize: AppSize.m),
            Center(
              child: AppButton.title(
                title: 'Reload',
                isLoading: false,
                onTapUp: onReload,
              ),
            )
          ]
        ],
      ),
    );
  }
}
