import 'package:flutter/material.dart';
import 'package:flutter_chat_types/flutter_chat_types.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:rly/design_system/app_button/app_button.dart';
import 'package:rly/design_system/app_separators.dart';
import 'package:rly/design_system/app_size.dart';
import 'package:rly/design_system/loader.widget.dart';
import 'package:rly/repository/entities/photo.entity.dart';
import 'package:rly/repository/entities/photo_chat.entity.dart';
import 'package:rly/repository/entities/photo_post.entity.dart';
import 'package:rly/repository/user.repository.dart';
import 'package:rly/screens/chat/chat.route.dart';
import 'package:rly/screens/common/widgets/dropdown/conversation/conversation_dropdown.widget.dart';
import 'package:rly/screens/common/widgets/dropdown/image_select_dropdown.widget.dart';
import 'package:rly/screens/common/widgets/notification_badge.widget.dart';
import 'package:rly/screens/common/widgets/photo.widget.dart';
import 'package:rly/screens/error/error.page.dart';
import 'package:rly/screens/helpers/context.extension.dart';
import 'package:rly/screens/loading/loading.page.dart';
import 'package:rly/screens/providers/photo_chat.provider.dart';
import 'package:rly/screens/providers/photo_post.provider.dart';
import 'package:rly/screens/publish/publish.page.dart';
import 'package:twemoji/twemoji.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:visibility_detector/visibility_detector.dart';

part 'tabs/my_rly.page.dart';
part 'tabs/friends_rly.page.dart';
part 'tabs/rly_preview.widget.dart';
part 'parts/add_button.widget.dart';

class DashboardPage extends ConsumerStatefulWidget {
  const DashboardPage({super.key});

  @override
  ConsumerState<DashboardPage> createState() => _DashboardPageState();
}

class _DashboardPageState extends ConsumerState<DashboardPage> {
  final authUserFuture = UserRepository.shared.getAuthUser();

  @override
  void initState() {
    super.initState();

    //hack in order to update the notification badge over the "from friends" tab (since the page is not loaded until the tab is tapped)
    WidgetsBinding.instance.addPostFrameCallback(
      (_) => ref.read(asyncPhotoChatProvider.notifier).fetchMorePhotoChats(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: authUserFuture,
        builder: (context, snapshot) {
          return switch (snapshot.connectionState) {
            ConnectionState.done => snapshot.hasData
                ? DefaultTabController(
                    length: 2,
                    child: Scaffold(
                      appBar: AppBar(
                        bottom: TabBar(
                          tabs: [
                            Tab(
                              text: 'from me',
                              icon: Visibility(
                                maintainAnimation: true,
                                maintainInteractivity: true,
                                maintainSize: true,
                                maintainState: true,
                                visible: _hasUnreadConversation(
                                    ref.watch(asyncPhotoPostProvider).value),
                                child: const NotificationBadgeWidget(
                                    size: AppSize.m),
                              ),
                            ),
                            Tab(
                              text: 'from friends',
                              icon: Visibility(
                                maintainAnimation: true,
                                maintainInteractivity: true,
                                maintainSize: true,
                                maintainState: true,
                                visible: _hasUnreadMessages(
                                    ref.watch(asyncPhotoChatProvider).value),
                                child: const NotificationBadgeWidget(
                                    size: AppSize.m),
                              ),
                            ),
                          ],
                        ),
                        title: TwemojiText(
                          text: 'RLY 📸',
                          style: Theme.of(context)
                              .appBarTheme
                              .titleTextStyle
                              ?.copyWith(
                                fontSize: (Theme.of(context)
                                            .appBarTheme
                                            .titleTextStyle
                                            ?.fontSize ??
                                        0) *
                                    2,
                              ),
                        ),
                      ),
                      body: LayoutBuilder(builder: (context, constraints) {
                        return TabBarView(
                          children: [
                            MyRLYPage(
                                authUser: snapshot.data!,
                                modalHeight: constraints.maxHeight * 0.8),
                            FriendsRLYPage(authUser: snapshot.data!),
                          ],
                        );
                      }),
                    ),
                  )
                // TODO: this should be a redirect to login page
                : const ErrorPage(errorMessage: 'user is not logged in'),
            _ => const LoadingPage(),
          };
        });
  }

  bool _hasUnreadConversation(List<PhotoPost>? photoPosts) {
    for (final photoPost in photoPosts ?? <PhotoPost>[]) {
      if (photoPost.unreadConversationsCount > 0) return true;
    }
    return false;
  }

  bool _hasUnreadMessages(List<PhotoChat>? photoChats) {
    for (final photoChat in photoChats ?? <PhotoChat>[]) {
      if (photoChat.hasUnreadMessages) return true;
    }
    return false;
  }
}
