class UserAuth {
  final String userId;
  final String token;

  const UserAuth({required this.userId, required this.token});
}
