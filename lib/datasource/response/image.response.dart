import 'package:mime/mime.dart';
import 'package:rly/datasource/response/response.dart';
import 'package:rly/repository/entities/photo.entity.dart';

class ImageResponse implements ApiResponse<Photo> {
  final ImageUrlResponse urls;
  final String id;

  ImageResponse({required this.id, required this.urls});

  factory ImageResponse.fromJson(Map<String, dynamic> json) {
    return ImageResponse(
        id: json['id'], urls: ImageUrlResponse.fromJson(json['urls']));
  }

  @override
  Photo get toEntity => Photo(
      id: id,
      contentType: lookupMimeType(urls.regular) ?? '',
      url: Uri.parse(urls.regular));
}

class ImageUrlResponse {
  final String regular;

  ImageUrlResponse({required this.regular});

  factory ImageUrlResponse.fromJson(Map<String, dynamic> json) {
    return ImageUrlResponse(regular: json['regular']);
  }
}
