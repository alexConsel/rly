import 'dart:math';

import 'package:collection/collection.dart';
import 'package:rly/datasource/request_param/create_photo_chat.request_param.dart';
import 'package:rly/datasource/response/message.response.dart';
import 'package:rly/datasource/response/photo.response.dart';
import 'package:rly/datasource/response/photo_chat.response.dart';
import 'package:rly/datasource/response/photo_post.response.dart';
import 'package:rly/datasource/response/user.response.dart';
import 'package:rly/repository/helper/map.extension.dart';

part 'parts/api.helpers.dart';
part 'parts/api.mock_data.dart';

class AppApi {
  static AppApi shared = AppApi._();

  AppApi._();

  Future<UserReponse> getUser(
          {required String id, required String? token}) async =>
      id == _currentUser.id
          ? _currentUser
          : _userReponses.firstWhere(
              (userResponse) => userResponse.id == id,
            );

  Future<void> readPhotoChat(
          {required String photoChatId, required String? token}) =>
      Future.value();

  Future<void> addTextMessage(
          {required String message, required String? token}) =>
      Future.value();

  Future<PhotoPostResponse> createPhotoPost(
      {required CreatePhotoChatRequestParam param, required String? token}) {
    final now = DateTime.now();
    final id = now.millisecondsSinceEpoch.toString();
    return Future.value(PhotoPostResponse(
      id: id,
      photo:
          PhotoResponse(id: id, contentType: 'image/jpg', url: param.photoUrl),
      updatedAt: DateTime.now(),
      photoChats: [],
      unreadConversationsCount: 0,
    ));
  }

  Future<List<PhotoChatResponse>> getPhotoChats(
          {required int page, required String? token}) async =>
      switch (page) {
        0 => _generatePhotoChatsFromPhotos(),
        _ => [],
      };

  Future<List<PhotoPostResponse>> getPhotoPosts(
          {required int page, required String? token}) async =>
      switch (page) {
        0 => _generatePhotoPosts(),
        _ => [],
      };
}
