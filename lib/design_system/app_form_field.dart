import 'package:flutter/material.dart';

class AppFormField extends StatelessWidget {
  final TextEditingController controller;
  final void Function() onEditingComplete;

  const AppFormField(
      {required this.onEditingComplete, required this.controller, super.key});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      autocorrect: false,
      style: Theme.of(context).textTheme.headlineLarge,
      textCapitalization: TextCapitalization.words,
      textInputAction: TextInputAction.next,
      validator: (value) => value == null || value.isEmpty
          ? 'This field should not be empty !'
          : null,
      onEditingComplete: onEditingComplete,
      decoration: const InputDecoration(border: InputBorder.none),
    );
  }
}
