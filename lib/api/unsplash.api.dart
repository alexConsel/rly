import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:rly/datasource/response/image.response.dart';

class UnsplashApi {
  static UnsplashApi shared = UnsplashApi._();

  UnsplashApi._();

  final String _accessKey = "8j7cdQS9HM1tfIomk_dUwqrIZ7wnymEECz6xk6OPP6k";
  final String _baseURL = "https://api.unsplash.com/";

  Future<List<ImageResponse>> getFeaturedPhotos() async {
    final url = photosUrl();
    final response = await http.get(url);
    if (response.statusCode == 200) {
      final List<dynamic> data = json.decode(response.body);
      List<ImageResponse> imageResponses =
          data.map((json) => ImageResponse.fromJson(json)).toList();
      return imageResponses;
    } else {
      throw Exception('Failed to load photos');
    }
  }

  Uri photosUrl() {
    return Uri.parse("${_baseURL}photos/?client_id=$_accessKey");
  }
}
