import 'package:rly/api/unsplash.api.dart';
import 'package:rly/datasource/response/image.response.dart';

class UnsplashDataSource {
  static UnsplashDataSource shared = UnsplashDataSource._();

  UnsplashDataSource._();

  Future<List<ImageResponse>> getFeaturedPhotos() =>
      UnsplashApi.shared.getFeaturedPhotos();
}
