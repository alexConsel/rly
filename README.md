# Alex Consel Submission - Voodoo iOS Founding Engineer Test

## App Introduction

Introducing RLY: the ultimate app tailored specifically for you and your friends. Say goodbye to the clutter of videos and audios – RLY keeps it simple, focusing solely on sharing meaningful moments through pictures.

With RLY, every image you share sparks a conversation. As soon as your friends react, a thread is born right beneath your masterpiece, allowing for seamless interaction and connection.

Experience the power of visual storytelling with RLY – where your pictures truly speak a thousand words.

## App Tour

- **"From Me" Tab**: This tab is where all your photos are shared.
- **"From Friends" Tab**: Lists all the conversations you've opened by reacting to a friend's photo.
- Use the floating action button to share a photo.
- Tapping on a shared photo opens a list of conversations from people who reacted to your photo.
- Notification badges indicate unopened conversations and are synchronized within the app.
- Conversations and photos are automatically sorted based on the last message sent/received; when you reply to a conversation, it will pop up all the way to the top.

## Shortcuts

- Users are logged in by default.
- The friends list is missing; we assume that people who react to your photos are your friends, and vice versa.
- There is no pagination when fetching data, for obvious simplicity reasons. Pagination could have been implemented when fetching messages, conversations, and photos.
- Notification badges are computed from the data we have (unread messages, unread conversations). Ideally, there would have been an endpoint to keep it in sync, especially with pagination in mind.
- You can only send texts in chats as I didn't have time to implement photo sharing.
- There are some code repetitions that could have been avoided by factorization; however, I did add TODOs for these.

## Notes

- I took more time to complete the test as I decided to experiment with a reactive caching framework called RiverPod. I also quite enjoyed the test and just messed around with the concept.

## Running the project

I recommend compiling the project on iOS using VSCode + XCode and iOS simulator

Don't hesitate to reach out to me in case you have any issue compiling the project

**⚠️ IMPORTANT: do not forget to run ``flutter pub get`` at project root level in order to be able compile the project. ⚠️**

Then you can run ``flutter run`` to launch it on your device or simulator.

```
XCode Version 15.2 (15C500b)
```

```
VSCode Version: 1.89.0 (Universal)
Commit: b58957e67ee1e712cebf466b995adf4c5307b2bd
Date: 2024-05-01T02:10:10.196Z
Electron: 28.2.8
ElectronBuildId: 27744544
Chromium: 120.0.6099.291
Node.js: 18.18.2
V8: 12.0.267.19-electron.0
OS: Darwin arm64 23.3.0
```

```
Flutter 3.19.5 • channel stable • [Flutter Repository](https://github.com/flutter/flutter.git)
Framework • revision 300451adae (6 weeks ago) • 2024-03-27 21:54:07 -0500
Engine • revision e76c956498
Tools • Dart 3.3.3 • DevTools 2.31.1
```