import 'package:flutter_chat_types/flutter_chat_types.dart';
import 'package:rly/datasource/response/response.dart';
import 'package:rly/datasource/response/user.response.dart';

class MessageResponse implements ApiResponse<Message> {
  final String id;
  final UserReponse author;
  final String body;
  final DateTime createdAt;

  const MessageResponse({
    required this.id,
    required this.author,
    required this.body,
    required this.createdAt,
  });

  @override
  Message get toEntity => TextMessage(
        author: author.toEntity,
        id: id,
        text: body,
        createdAt: createdAt.millisecondsSinceEpoch,
      );
}
