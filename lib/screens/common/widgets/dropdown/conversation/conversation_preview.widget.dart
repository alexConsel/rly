part of 'conversation_dropdown.widget.dart';

class _ConversationPreviewWidget extends StatelessWidget {
  final PhotoChat chat;

  const _ConversationPreviewWidget({required this.chat, super.key});

  @override
  Widget build(BuildContext context) {
    final height = AppSize.xl.value;
    final avatarSize = height - AppSize.m.value * 2;
    return SizedBox(
      height: height,
      child: Row(
        children: [
          Center(
            child: PhotoWidget(
              url: chat.user.imageUrl,
              width: avatarSize,
              height: avatarSize,
              borderRadius: BorderRadius.circular(avatarSize / 2),
            ),
          ),
          AppHorizontalSeparator.fromAppSize(appSize: AppSize.s),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  chat.user.firstName ?? '',
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(
                  chat.lastTextMessage?.text ?? '',
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
          ),
          AppHorizontalSeparator.fromAppSize(appSize: AppSize.s),
          if (chat.lastMessage?.createdAt != null) ...[
            Opacity(
              opacity: 0.5,
              child: Text(
                timeago.format(
                  DateTime.fromMillisecondsSinceEpoch(
                    chat.lastMessage!.createdAt!,
                  ),
                ),
              ),
            ),
            AppHorizontalSeparator.fromAppSize(appSize: AppSize.s)
          ],
          if (chat.hasUnreadMessages)
            const NotificationBadgeWidget(
              size: AppSize.m,
            ),
        ],
      ),
    );
  }
}
