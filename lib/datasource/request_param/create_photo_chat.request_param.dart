class CreatePhotoChatRequestParam {
  final String photoUrl;

  const CreatePhotoChatRequestParam({required this.photoUrl});
}
