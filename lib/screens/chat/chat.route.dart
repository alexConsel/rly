import 'package:flutter_chat_types/flutter_chat_types.dart';
import 'package:go_router/go_router.dart';
import 'package:rly/app/app_route.dart';
import 'package:rly/repository/entities/photo.entity.dart';
import 'package:rly/screens/chat/chat.page.dart';

class ChatRoute extends AppRoute {
  @override
  String get path => '/chat';

  @override
  GoRoute get route => GoRoute(
        path: path,
        builder: (context, state) => ChatPage(
          pageParam: state.extra as ChatPageParam,
        ),
      );
}

sealed class ChatPageParam {
  final String? _postId;
  final String chatId;
  final Photo photo;
  final User authUser;

  const ChatPageParam(this.photo, this.chatId, this.authUser, this._postId);
}

class PhotoPostParam extends ChatPageParam {
  String get postId => _postId!;

  PhotoPostParam(
      {required String chatId,
      required Photo photo,
      required String postId,
      required User authUser})
      : super(photo, chatId, authUser, postId);
}

class PhotoChatParam extends ChatPageParam {
  PhotoChatParam(
      {required String chatId, required Photo photo, required User authUser})
      : super(photo, chatId, authUser, null);
}
