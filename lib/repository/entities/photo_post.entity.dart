import 'package:rly/repository/entities/photo.entity.dart';
import 'package:rly/repository/entities/photo_chat.entity.dart';

class PhotoPost {
  final String id;
  final List<PhotoChat> photoChats;
  final Photo photo;
  final DateTime updatedAt;
  final int unreadConversationsCount;

  const PhotoPost({
    required this.id,
    required this.photoChats,
    required this.photo,
    required this.updatedAt,
    required this.unreadConversationsCount,
  });

  PhotoPost copyWith({
    String? id,
    List<PhotoChat>? photoChats,
    Photo? photo,
    DateTime? updatedAt,
    int? unreadConversationsCount,
  }) {
    return PhotoPost(
      id: id ?? this.id,
      photoChats: photoChats ?? this.photoChats,
      photo: photo ?? this.photo,
      updatedAt: updatedAt ?? this.updatedAt,
      unreadConversationsCount:
          unreadConversationsCount ?? this.unreadConversationsCount,
    );
  }
}
